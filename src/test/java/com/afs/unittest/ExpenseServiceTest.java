package com.afs.unittest;

import com.afs.unittest.exception.ProjectExpiredException;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project internalProject = new Project(ProjectType.INTERNAL, "project name");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(internalProject);

        // then
        assertEquals(ExpenseType.INTERNAL_PROJECT_EXPENSE, actual);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project externalProjectA = new Project (ProjectType.EXTERNAL, "Project A");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(externalProjectA);

        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_A, actual);
    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project externalProjectB = new Project (ProjectType.EXTERNAL, "Project B");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(externalProjectB);

        // then
        assertEquals(ExpenseType.EXPENSE_TYPE_B, actual);
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project externalProjectother = new Project (ProjectType.EXTERNAL, "project name");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(externalProjectother);

        // then
        assertEquals(ExpenseType.OTHER_EXPENSE, actual);
    }

    //optional
    @Test
    void should_throw_unexpected_project_exception_when_expectedProjectTypeException_given_project_is_invalid() {
        // given
        Project project = new Project (ProjectType.UNEXPECTED_PROJECT_TYPE, "project name");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        assertThrows(UnexpectedProjectTypeException.class, () -> expenseService.getExpenseCodeByProject(project));

        // then
//        assertEquals(ExpenseType.UNEXPECTED_PROJECT_TYPE, actual);
    }

    @Test
    void should_return_submit_state_when_submit_expense_given_project_do_not_expired() {
        // given
        Project project = new Project (ProjectType.EXTERNAL, "project name");
        IProjectClient fakeprojectClient = new FakeProjectClient();
        ExpenseService expenseService = new ExpenseService(fakeprojectClient);

        // when
        ExpenseState actual = expenseService.submitExpense(project);

        // then
        assertEquals(ExpenseState.SUBMIT, actual);
    }

    @Test
    void should_throw_expired_exception_when_submit_expense_given_project_has_expired() {
        // given
        Project project = new Project (ProjectType.EXTERNAL, "project name");

        ProjectClient mockProjectClient = mock(ProjectClient.class);
        when(mockProjectClient.isExpired(project)).thenReturn(true);
        ExpenseService expenseService = new ExpenseService(mockProjectClient);
        // when
        Executable executable = () -> expenseService.submitExpense(project);
        // then
        assertThrows(ProjectExpiredException.class, executable);
    }
}
