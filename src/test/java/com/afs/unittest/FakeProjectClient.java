package com.afs.unittest;

public class FakeProjectClient implements IProjectClient {
    @Override
    public boolean isProjectTypeValid(ProjectType projectType) {
        return false;
    }

    @Override
    public boolean isExpired(Project project) {
        return false;
    }
}
