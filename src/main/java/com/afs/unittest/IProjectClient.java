package com.afs.unittest;

public interface IProjectClient {
    boolean isProjectTypeValid(ProjectType projectType);

    boolean isExpired(Project project);
}
